# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Utility to verify Xbox 360 ISOs against an online database."
HOMEPAGE="http://abgx360.xecuter.com/"
SRC_URI="http://dl.dropbox.com/u/59058148/${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="net-misc/curl 
sys-libs/zlib"

src_compile() {
  econf
  emake || die "emake failed"
}

src_install() {
  emake DESTDIR="${D}" install || die "Install failed"
}

pkg_postinst() {
	elog "Be sure to check $HOMEPAGE for Video ISOs download."
}
